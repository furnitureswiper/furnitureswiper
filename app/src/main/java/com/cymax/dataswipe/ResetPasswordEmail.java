package com.cymax.dataswipe;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.cymax.dataswipe.helper.Helper;
import com.cymax.dataswipe.model.DeviceData;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.io.StringReader;
import java.security.Key;
import java.security.spec.KeySpec;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

public class ResetPasswordEmail extends AppCompatActivity {

    private Button btnEmail;
    private EditText txtEmail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        ProgressDialog pDialog;

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_password_email);

        txtEmail = (EditText) findViewById(R.id.txtEmail);
        btnEmail = (Button) findViewById(R.id.btnEmail);
        btnEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isEmpty(txtEmail))
                    sendEmail(txtEmail.getText().toString().trim());
            }
        });

        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Loading.....");
        pDialog.setCancelable(false);
    }

    private void sendEmail(String email) {

        String url = DeviceData.getById(1).getUrlBaseAddress() + "/ResetPasswordEmail/" + email;

        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
                        DocumentBuilder builder;
                        InputSource is;
                        try {
                            builder = factory.newDocumentBuilder();
                            is = new InputSource(new StringReader(response));
                            Document doc = builder.parse(is);
                            NodeList list = doc.getElementsByTagName("string");
                            String result = list.item(0).getTextContent();

                            if (result.equals("successful")) {
                                Toast.makeText(ResetPasswordEmail.this, "An email with your new password was sent to: " + txtEmail.getText().toString().trim(), Toast.LENGTH_LONG).show();
                                Intent intent = new Intent(ResetPasswordEmail.this, SplashActivity.class);
                                startActivity(intent);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(),"error",Toast.LENGTH_LONG).show();
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                try {
                    Date date = new Date();
                    Map<String, String> headers = new HashMap<>();
                    headers.put("Authorization", Helper.generateRequestToken(date));
                    headers.put("Date", date.toString());
                    return headers;
                } catch (Exception e) {
                    Log.e("Authorization", "Authentication Failure");
                }
                return super.getParams();
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        // Add the request to the RequestQueue.
        queue.add(stringRequest);
   }

    // check if edittext is empty
    private boolean isEmpty(EditText etText) {
        if (etText.getText().toString().trim().length() == 0) {
            return true;
        } else {
            return false;
        }
    }
}
