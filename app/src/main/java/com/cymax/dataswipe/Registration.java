package com.cymax.dataswipe;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.cymax.dataswipe.helper.Helper;
import com.cymax.dataswipe.model.DeviceData;
import com.cymax.dataswipe.model.UserData;

import org.json.JSONObject;
import org.json.JSONStringer;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import java.io.StringReader;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

public class Registration extends AppCompatActivity implements View.OnClickListener {

    private EditText textEmail;
    private EditText textTurk;
    private EditText textPassword;
    private Button buttonRegister;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        ProgressDialog pDialog;

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        textEmail = (EditText) findViewById(R.id.editTextEmail);
        textTurk = (EditText) findViewById(R.id.editTextTurk);
        textPassword = (EditText) findViewById(R.id.editTextPassword);

        buttonRegister = (Button) findViewById(R.id.btnRegister);
        buttonRegister.setOnClickListener(this);

        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Loading.....");
        pDialog.setCancelable(false);
    }

    @Override
    public void onClick(View v) {
        if (v == buttonRegister) {
            if (!isEmpty(textEmail, textTurk, textPassword))
                registerUser();
        }
    }

    private void registerUser() {
        String email = textEmail.getText().toString().trim();
        String turk = textTurk.getText().toString().trim();
        String password = textPassword.getText().toString().trim();
        register(email, turk, password);
    }

    private void register(String email, String turk, String password) {

        DeviceData dv = DeviceData.getById(1);
        String url = dv.getUrlBaseAddress() + "/RegisterUser";
        final JSONObject entity;

        // Build JSON array
        JSONStringer reviewJObj;
        try {
            reviewJObj = new JSONStringer()
                    .object()
                    .key("email").value(email)
                    .key("turk").value(turk)
                    .key("password").value(Helper.encryptedPassword(password))
                    .endObject();

            entity = new JSONObject(reviewJObj.toString());

            JsonObjectRequest req;

            req = new JsonObjectRequest( Request.Method.POST, url,entity,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {

                            try {
                                if (response.getString("result").contains("successful")) {
                                    Toast.makeText(getApplicationContext(), "Registration Successful", Toast.LENGTH_LONG).show();

                                    // remove current last user
                                    if(UserData.getById(1) != null) UserData.deletebyId(1);

                                    Intent intent = new Intent(Registration.this, SplashActivity.class);
                                    startActivity(intent);
                                } else {
                                    Toast.makeText(getApplicationContext(), "Registration Failed", Toast.LENGTH_LONG).show();
                                }
                            }
                            catch (Exception ex)
                            {
                                ex.printStackTrace();
                                Toast.makeText(getApplicationContext(), "Invalid server response", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(getApplicationContext(), "The server refused the new password!", Toast.LENGTH_LONG).show();
                }
            }){
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    try {
                        Date date = new Date();
                        Map<String, String> headers = new HashMap<>();
                        headers.put("Authorization", Helper.generateRequestToken(date));
                        headers.put("Date", date.toString());
                        return headers;
                    } catch (Exception e) {
                        Log.e("Authorization", "Authentication Failure");
                    }
                    return super.getParams();
                }
            };

            // Adding request to request queue
            AppController.getInstance().addToRequestQueue(req);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // check if edittext is empty
    private boolean isEmpty(EditText etText, EditText etText1, EditText etText2) {
        return (etText.getText().toString().trim().length() == 0 || etText1.getText().toString().trim().length() == 0 || etText2.getText().toString().trim().length() == 0);
    }
}

