    package com.cymax.dataswipe.swipelistview;

    import android.content.Context;
import android.support.v4.view.GestureDetectorCompat;
import android.support.v4.widget.ScrollerCompat;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.GestureDetector.OnGestureListener;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Interpolator;
import android.widget.AbsListView;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cymax.dataswipe.MainActivity;
import com.cymax.dataswipe.R;

    public class SwipeMenuLayout extends FrameLayout {

        private static final int CONTENT_VIEW_ID = 1;
        private static final int MENU_VIEW_ID = 2;

        private static final int STATE_CLOSE = 0;
        private static final int STATE_OPEN = 1;

        private int mSwipeDirection;

        private View mContentView;
        private SwipeMenuView mMenuView;
        private int mDownX;
        private int state = STATE_CLOSE;
        private GestureDetectorCompat mGestureDetector;
        private OnGestureListener mGestureListener;
        private boolean isFling;
        private int MIN_FLING = dp2px(15);
        private int MAX_VELOCITYX = -dp2px(500);
        private ScrollerCompat mOpenScroller;
        private ScrollerCompat mCloseScroller;
        private int mBaseX;
        private int position;
        private Interpolator mCloseInterpolator;
        private Interpolator mOpenInterpolator;
        private int oldDirection = 0;
        private boolean addAttribue = true;

        public SwipeMenuLayout(View contentView, SwipeMenuView menuView) {
            this(contentView, menuView, null, null);
        }

        public SwipeMenuLayout(View contentView, SwipeMenuView menuView,Interpolator closeInterpolator, Interpolator openInterpolator) {
            super(contentView.getContext());
            mCloseInterpolator = closeInterpolator;
            mOpenInterpolator = openInterpolator;
            mContentView = contentView;
            setmMenuView(menuView);
            getmMenuView().setLayout(this);
            init();
        }

        private SwipeMenuLayout(Context context, AttributeSet attrs) {
            super(context, attrs);
        }

        private SwipeMenuLayout(Context context) {
            super(context);
        }

        public int getPosition() {
            return position;
        }

        public void setPosition(int position) {
            this.position = position;
            getmMenuView().setPosition(position);
        }

        public void setSwipeDirection(int swipeDirection) {
            mSwipeDirection = swipeDirection;

        }

        @SuppressWarnings("ResourceType")
        private void init() {
            setLayoutParams(new AbsListView.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
            mGestureListener = new SimpleOnGestureListener() {
                @Override
                public boolean onDown(MotionEvent e) {
                    isFling = false;
                    return true;
                }

                @Override
                public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {

                    if (Math.abs(e1.getX() - e2.getX()) > MIN_FLING
                            && velocityX < MAX_VELOCITYX) {
                        isFling = true;
                    }
                    return super.onFling(e1, e2, velocityX, velocityY);
                }
            };

            mGestureDetector = new GestureDetectorCompat(getContext(), mGestureListener);

            if (mCloseInterpolator != null) {
                mCloseScroller = ScrollerCompat.create(getContext(), mCloseInterpolator);
            } else {
                mCloseScroller = ScrollerCompat.create(getContext());
            }
            if (mOpenInterpolator != null) {
                mOpenScroller = ScrollerCompat.create(getContext(), mOpenInterpolator);
            } else {
                mOpenScroller = ScrollerCompat.create(getContext());
            }

            LayoutParams contentParams = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
            mContentView.setLayoutParams(contentParams);
            if (mContentView.getId() < 1) {
                mContentView.setId(CONTENT_VIEW_ID);
            }

            getmMenuView().setId(MENU_VIEW_ID);
            getmMenuView().setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT));

            addView(mContentView);
            addView(getmMenuView());

        }

        @Override
        protected void onAttachedToWindow() {
            super.onAttachedToWindow();
        }

        @Override
        protected void onSizeChanged(int w, int h, int oldw, int oldh) {
            super.onSizeChanged(w, h, oldw, oldh);
        }

        public boolean onSwipe(MotionEvent event) {
            mGestureDetector.onTouchEvent(event);

            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:

                    mMenuView.setVisibility(VISIBLE);

                    if(oldDirection != mSwipeDirection)
                    if (mSwipeDirection == SwipeListView.DIRECTION_LEFT) {
                        ((LinearLayout)mMenuView.getChildAt(0)).setBackgroundDrawable(getResources().getDrawable(R.drawable.gradient_add));
                        ((TextView)((LinearLayout)mMenuView.getChildAt(0)).getChildAt(0)).setText("ADD");
                        addAttribue = true;
                    }
                    else{
                        ((LinearLayout)mMenuView.getChildAt(0)).setBackgroundDrawable(getResources().getDrawable(R.drawable.gradient_remove));
                        ((TextView)((LinearLayout)mMenuView.getChildAt(0)).getChildAt(0)).setText("REMOVE");
                        addAttribue = false;
                    }
                    oldDirection = mSwipeDirection;

                    mDownX = (int) event.getX();
                    isFling = false;

                    break;
                case MotionEvent.ACTION_MOVE:

                    int dis = (int) (mDownX - event.getX());
                    swipe(dis);

                    ((LinearLayout)mMenuView.getChildAt(0)).setAlpha(Math.abs(mDownX - event.getX()) / getmMenuView().getWidth());
                    ((TextView)((LinearLayout)mMenuView.getChildAt(0)).getChildAt(0)).setAlpha(Math.abs(mDownX - event.getX()) / getmMenuView().getWidth());

                    break;


                case MotionEvent.ACTION_UP:

                    if ((isFling || Math.abs(mDownX - event.getX()) > (getmMenuView().getWidth() / 1.4)) &&
                            Math.signum(mDownX - event.getX()) == mSwipeDirection) {
                       // open
                        openMenu();//smoothOpenMenu();
                        if (isOpen() &&  mContentView.getParent().getParent() != null) {
                            mMenuView.setVisibility(INVISIBLE);
                            closeMenu();//quickCloseMenu();
                            ((MainActivity) ((RelativeLayout) mContentView.getParent().getParent().getParent()).getContext()).saveAttributeListItem(position, addAttribue);
                        }
                        else
                        {
                            smoothCloseMenu();
                        }
                    }
                    else {
                        smoothCloseMenu();
                    }
                    break;
            }
            return true;
        }

        public boolean isOpen() {
            return state == STATE_OPEN;
        }

        @Override
        public boolean onTouchEvent(MotionEvent event) {

            return super.onTouchEvent(event);
        }

        private void swipe(int dis) {

            if (Math.signum(dis) != mSwipeDirection) {
                dis =0;
            } else if (Math.abs(dis) > getmMenuView().getWidth()) {
                dis = getmMenuView().getWidth()*mSwipeDirection;
            }

            mContentView.layout(-dis, mContentView.getTop(), mContentView.getWidth() -dis, getMeasuredHeight());

            if (mSwipeDirection == SwipeListView.DIRECTION_LEFT) {

                getmMenuView().layout(mContentView.getWidth() - dis, getmMenuView().getTop(),
                        mContentView.getWidth() + getmMenuView().getWidth() - dis,
                        getmMenuView().getBottom());
            } else {
                getmMenuView().layout(-getmMenuView().getWidth() - dis, getmMenuView().getTop(), - dis, getmMenuView().getBottom());
            }
            postInvalidate();
        }

        @Override
        public void computeScroll() {
            if (state == STATE_OPEN) {
                if (mOpenScroller.computeScrollOffset()) {
                    swipe(mOpenScroller.getCurrX()*mSwipeDirection);
                    postInvalidate();
                }
            } else {
                if (mCloseScroller.computeScrollOffset()) {
                    swipe((mBaseX - mCloseScroller.getCurrX())*mSwipeDirection);
                    postInvalidate();
                }
            }
        }

        public void smoothCloseMenu() {

            //if(state != STATE_CLOSE) {
                state = STATE_CLOSE;
                if (mSwipeDirection == SwipeListView.DIRECTION_LEFT) {
                    mBaseX = -mContentView.getLeft();
                    mCloseScroller.startScroll(0, 0, getmMenuView().getWidth(), 0, 350);
                } else {
                    mBaseX = getmMenuView().getRight();
                    mCloseScroller.startScroll(0, 0, getmMenuView().getWidth(), 0, 350);
                }
                postInvalidate();
            //}
        }

        public void quickCloseMenu() {
            //if(state != STATE_CLOSE) {
                state = STATE_CLOSE;
                if (mSwipeDirection == SwipeListView.DIRECTION_LEFT) {
                    mBaseX = -mContentView.getLeft();
                    mCloseScroller.startScroll(0, 0, getmMenuView().getWidth(), 0, 1);
                } else {
                    mBaseX = getmMenuView().getRight();
                    mCloseScroller.startScroll(0, 0, getmMenuView().getWidth(), 0, 1);
                }
                postInvalidate();
            //}
        }

        public void smoothOpenMenu() {
            if(state != STATE_OPEN) {
                state = STATE_OPEN;
                if (mSwipeDirection == SwipeListView.DIRECTION_LEFT) {
                    mOpenScroller.startScroll(-mContentView.getLeft(), 0, getmMenuView().getWidth(), 0, 150);
                } else {
                    mOpenScroller.startScroll(mContentView.getLeft(), 0, getmMenuView().getWidth(), 0, 150);
                }
                postInvalidate();
            }
        }

        public void closeMenu() {
            if (mCloseScroller.computeScrollOffset()) {
                mCloseScroller.abortAnimation();
            }
            if (state == STATE_OPEN) {
                state = STATE_CLOSE;
                swipe(0);
            }
        }

        public void openMenu() {
            if (state == STATE_CLOSE) {
                state = STATE_OPEN;
                swipe(getmMenuView().getWidth()*mSwipeDirection);
            }
        }

        public View getContentView() {
            return mContentView;
        }

        public SwipeMenuView getMenuView() {
            return getmMenuView();
        }

        private int dp2px(int dp) {
            return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp,
                    getContext().getResources().getDisplayMetrics());
        }

        @Override
        protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
            super.onMeasure(widthMeasureSpec, heightMeasureSpec);
            getmMenuView().measure(MeasureSpec.makeMeasureSpec(0,
                    MeasureSpec.UNSPECIFIED), MeasureSpec.makeMeasureSpec(
                    getMeasuredHeight(), MeasureSpec.EXACTLY));
        }

        @Override
        protected void onLayout(boolean changed, int l, int t, int r, int b) {
            mContentView.layout(0, 0, getMeasuredWidth(),
                    mContentView.getMeasuredHeight());
            if (mSwipeDirection == SwipeListView.DIRECTION_LEFT) {
                getmMenuView().layout(getMeasuredWidth(), 0,
                        getMeasuredWidth() + getmMenuView().getMeasuredWidth(),
                        mContentView.getMeasuredHeight());
            } else {
                getmMenuView().layout(-getmMenuView().getMeasuredWidth(), 0,
                        0, mContentView.getMeasuredHeight());
            }
        }

        public void setMenuHeight(int measuredHeight) {
            LayoutParams params = (LayoutParams) getmMenuView().getLayoutParams();
            if (params.height != measuredHeight) {
                params.height = measuredHeight;
                getmMenuView().setLayoutParams(getmMenuView().getLayoutParams());
            }
        }

        public SwipeMenuView getmMenuView() {
            return mMenuView;
        }

        public void setmMenuView(SwipeMenuView mMenuView) {
            this.mMenuView = mMenuView;
        }
    }
