package com.cymax.dataswipe.swipelistview;

public interface SwipeMenuCreator {

	void create(SwipeMenu menu);
}
