    package com.cymax.dataswipe.swipelistview;

    import android.content.Context;
    import android.database.DataSetObserver;
    import android.graphics.Color;
    import android.graphics.drawable.ColorDrawable;
    import android.support.v4.view.MotionEventCompat;
    import android.util.AttributeSet;
    import android.util.TypedValue;
    import android.view.GestureDetector;
    import android.view.MotionEvent;
    import android.view.View;
    import android.view.animation.Interpolator;
    import android.widget.Adapter;
    import android.widget.LinearLayout;
    import android.widget.ListAdapter;
    import android.widget.ListView;
    import android.widget.RelativeLayout;
    import android.widget.TextView;

    import com.cymax.dataswipe.MainActivity;
    import com.cymax.dataswipe.adapter.AttributesAdapter;

    public class SwipeListView extends ListView {

        private static final int TOUCH_STATE_NONE = 0;
        private static final int TOUCH_STATE_X = 1;
        private static final int TOUCH_STATE_Y = 2;

        public static final int DIRECTION_LEFT = 1;
        public static final int DIRECTION_RIGHT = -1;
        private int mDirection = -1;//swipe from right to left by default

        private int MAX_Y = 5;
        private int MAX_X = 3;
        private float mDownX;
        private float oldX =-999;
        private float mDownY;
        private int mTouchState;
        private int mTouchPosition;
        private SwipeMenuLayout mTouchView;
        private OnSwipeListener mOnSwipeListener;

        private SwipeMenuCreator mMenuCreator;
        private OnMenuItemClickListener mOnMenuItemClickListener;
        private Interpolator mCloseInterpolator;
        private Interpolator mOpenInterpolator;
        private ListAdapter mAdapter;
        private AdapterDataSetObserver mDataSetObserver;

        public SwipeListView(Context context) {
            super(context);
            init();
            setAnimationCacheEnabled(false);
            setScrollingCacheEnabled(false);
            setChildrenDrawingCacheEnabled(false);
            setChildrenDrawnWithCacheEnabled(false);
        }

        public SwipeListView(Context context, AttributeSet attrs, int defStyle) {
            super(context, attrs, defStyle);
            init();
            setAnimationCacheEnabled(false);
            setScrollingCacheEnabled(false);
            setChildrenDrawingCacheEnabled(false);
            setChildrenDrawnWithCacheEnabled(false);
        }

        public SwipeListView(Context context, AttributeSet attrs) {
            super(context, attrs);
            init();
            setAnimationCacheEnabled(false);
            setScrollingCacheEnabled(false);
            setChildrenDrawingCacheEnabled(false);
            setChildrenDrawnWithCacheEnabled(false);
        }

        private void init() {
            MAX_X = dp2px(MAX_X);
            MAX_Y = dp2px(MAX_Y);
            mTouchState = TOUCH_STATE_NONE;
        }

        public void setCloseInterpolator(Interpolator interpolator) {
            mCloseInterpolator = interpolator;
        }

        public void setOpenInterpolator(Interpolator interpolator) {
            mOpenInterpolator = interpolator;
        }

        public Interpolator getOpenInterpolator() {
            return mOpenInterpolator;
        }

        public Interpolator getCloseInterpolator() {
            return mCloseInterpolator;
        }

        @Override
        public boolean onInterceptTouchEvent(MotionEvent ev) {
            return super.onInterceptTouchEvent(ev);
        }

        @Override
        public boolean onTouchEvent(MotionEvent ev) {
            if (ev.getAction() != MotionEvent.ACTION_DOWN && mTouchView == null)
                return super.onTouchEvent(ev);
            int action = MotionEventCompat.getActionMasked(ev);
            action = ev.getAction();

            if (ev.getX() > oldX )
                mDirection = DIRECTION_LEFT;
            else
                mDirection = DIRECTION_RIGHT;

            switch (action) {
            case MotionEvent.ACTION_DOWN:

                int oldPos = mTouchPosition;
                oldX =ev.getX();

                mDownX = ev.getX() ;
                mDownY = ev.getY();
                mTouchState = TOUCH_STATE_NONE;

                mTouchPosition = pointToPosition((int) ev.getX(), (int) ev.getY());

                if (mTouchPosition == oldPos && mTouchView != null
                        && mTouchView.isOpen()) {
                    mTouchState = TOUCH_STATE_X;
                    mTouchView.onSwipe(ev);
                    return true;
                }

                View view = getChildAt(mTouchPosition - getFirstVisiblePosition());

//                if (mTouchView != null && mTouchView.isOpen()) {
//                    mTouchView.smoothCloseMenu();
//                    mTouchView = null;
//                    // try to cancel the touch event
//                    MotionEvent cancelEvent = MotionEvent.obtain(ev);
//                    cancelEvent.setAction(MotionEvent.ACTION_CANCEL);
//                    onTouchEvent(cancelEvent);
//                    return true;
//                }
                if (view instanceof SwipeMenuLayout) {
                    mTouchView = (SwipeMenuLayout) view;
                    mTouchView.setSwipeDirection(mDirection);
                }
                if (mTouchView != null) {
                    mTouchView.onSwipe(ev);
                }
                break;
            case MotionEvent.ACTION_MOVE:

                // get diretction
                if (ev.getX() > mDownX )
                    mDirection = DIRECTION_LEFT;
                else
                    mDirection = DIRECTION_RIGHT;


                float dy = Math.abs((ev.getY() - mDownY));
                float dx = Math.abs((ev.getX() - mDownX));

                if (mTouchState == TOUCH_STATE_X) {
                    if (mTouchView != null) {
                        mTouchView.onSwipe(ev);
                    }
                    getSelector().setState(new int[] { 0 });
                    ev.setAction(MotionEvent.ACTION_CANCEL);
                    super.onTouchEvent(ev);
                    return true;
                } else if (mTouchState == TOUCH_STATE_NONE) {
                    if (Math.abs(dy) > MAX_Y) {
                        mTouchState = TOUCH_STATE_Y;
                    } else if (dx > MAX_X) {
                        mTouchState = TOUCH_STATE_X;
                        if (mOnSwipeListener != null) {
                            mOnSwipeListener.onSwipeStart(mTouchPosition);
                        }
                    }
                }
                break;
            case MotionEvent.ACTION_UP:
//                mDownX = oldX;
                if (mTouchState == TOUCH_STATE_X) {
                    if (mTouchView != null) {
                        mTouchView.onSwipe(ev);
                        if (!mTouchView.isOpen()) {
                            mTouchView = null;
                        }
                    }
                    if (mOnSwipeListener != null) {
                        mOnSwipeListener.onSwipeEnd(mTouchPosition);
                        postInvalidate();
                    }
                    ev.setAction(MotionEvent.ACTION_CANCEL);
                }
                break;
            }

            oldX = ev.getX();

            return super.onTouchEvent(ev);
        }

        private int dp2px(int dp) {
            return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp,
                    getContext().getResources().getDisplayMetrics());
        }

        public void setMenuCreator(SwipeMenuCreator menuCreator) {
            this.mMenuCreator = menuCreator;
        }

        public void setOnMenuItemClickListener(
                OnMenuItemClickListener onMenuItemClickListener) {
            this.mOnMenuItemClickListener = onMenuItemClickListener;
        }

        public void setOnSwipeListener(OnSwipeListener onSwipeListener) {
            this.mOnSwipeListener = onSwipeListener;
        }

        public static interface OnMenuItemClickListener {
            boolean onMenuItemClick(int position, SwipeMenu menu, int index);
        }

        public static interface OnSwipeListener {
            void onSwipeStart(int position);

            void onSwipeEnd(int position);
        }

        public void setSwipeDirection(int direction) {
            mDirection = direction;
        }

        @Override
        public ListAdapter getAdapter() {
            return mAdapter;
        }


        @Override
        public void setAdapter(ListAdapter adapter) {

            super.setAdapter(new SwipeMenuAdapter(getContext(), adapter) {
                @Override
                public void createMenu(SwipeMenu menu) {
                    if (mMenuCreator != null) {
                        mMenuCreator.create(menu);
                    }
                }

                @Override
                public void onItemClick(SwipeMenuView view, SwipeMenu menu,
                                        int index) {
//                    boolean flag = false;
//                    if (mOnMenuItemClickListener != null) {
//                        flag = mOnMenuItemClickListener.onMenuItemClick(
//                                view.getPosition(), menu, index);
//                    }
//                    if (mTouchView != null && !flag) {
//                        mTouchView.smoothCloseMenu();
//                    }
                }
            });

            if (mAdapter != null && mDataSetObserver != null) {
                mAdapter.unregisterDataSetObserver(mDataSetObserver);
                mDataSetObserver = null;
            }

            mAdapter = adapter;
//
            if (mAdapter != null && mDataSetObserver == null) {
                mDataSetObserver = new AdapterDataSetObserver();
                mAdapter.registerDataSetObserver(mDataSetObserver);
            }
        }

        private class AdapterDataSetObserver extends DataSetObserver {
            @Override
            public void onChanged() {
                requestLayout();
                super.onChanged();
            }

            @Override
            public void onInvalidated() {
                requestLayout(); super.onInvalidated();
            }

        }
    }
