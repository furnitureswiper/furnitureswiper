package com.cymax.dataswipe.model;

import com.j256.ormlite.field.DatabaseField;

/**
 * A simple demonstration object we are creating and persisting to the database.
 */
public class AttributeData extends DefaultData{

	@DatabaseField
    private String category;
    @DatabaseField
    private String attributeGroup;
    @DatabaseField
    private String attribute;
    @DatabaseField
    private String parentCategory;
    @DatabaseField
    private boolean attributeAdded;

	public AttributeData() {
		// needed by ormlite
	}

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getAttributeGroup() {
        return attributeGroup;
    }

    public void setAttributeGroup(String attributeGroup) {
        this.attributeGroup = attributeGroup;
    }

    public String getAttribute() {
        return attribute;
    }

    public void setAttribute(String attribute) {
        this.attribute = attribute;
    }

    public String getParentCategory() {
        return parentCategory;
    }

    public void setParentCategory(String parentCategory) {
        this.parentCategory = parentCategory;
    }

    public boolean isAttributeAdded() {
        return attributeAdded;
    }

    public void setAttributeAdded(boolean attributeAdded) {
        this.attributeAdded = attributeAdded;
    }
}
