package com.cymax.dataswipe.model;

import android.util.Log;

import com.cymax.dataswipe.db.DatabaseManager;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.sql.SQLException;
import java.util.List;

@DatabaseTable(tableName = "user")
public class UserData extends DefaultData {

	private static final String USERNAME_FIELD_NAME = "username";
	@DatabaseField(index = true, columnName = USERNAME_FIELD_NAME)
	private String username;
	@DatabaseField
	private String password;
	@DatabaseField
	private String turkAccount;
	@DatabaseField
	private boolean rememberPassword;
	@DatabaseField
	private int serverUserId;

	// necessary for ormLite
	public UserData (){}

	public static String getUsernameFieldName() {
		return USERNAME_FIELD_NAME;
	}

	public int insert() {
		try {
			 DatabaseManager.getUserDataDao().create(this);
			 return this.id;
		} catch (SQLException e) {
			Log.e(UserData.class.getName(), "insert", e);
			return -1;
		}
	}

	public static int deletebyId(int userId) {
		try {
			DatabaseManager.getUserDataDao().deleteById(userId);
			return userId;
		} catch (SQLException e) {
			Log.e(UserData.class.getName(), "deletebyId", e);
			return -1;
		}
	}

	static public UserData getByUsername(String userName)
	{
		List<UserData> userList;
		try {
			userList =  DatabaseManager.getUserDataDao().queryForEq(getUsernameFieldName(), userName);
			if (!userList.isEmpty()) return userList.get(0);
			
		} catch (SQLException e) {
			Log.e(UserData.class.getName(), "getByUsername", e);
		}
		return null;
	}

	static public List<UserData> getAll()
	{
		List<UserData> userList = null;
		try {
		
			userList =  DatabaseManager.getUserDataDao().queryForAll();

		} catch (SQLException e) {
			Log.e(UserData.class.getName(), "getAll", e);
		}
		
		return userList;
	}

	static public UserData getById(int userId) {
		UserData user = null;
		try {
			user = DatabaseManager.getUserDataDao().queryForId(userId);
		} catch (SQLException e) {
			Log.e(UserData.class.getSimpleName(), "getById", e);
		}
		return user;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getTurkAccount() {
		return turkAccount;
	}

	public void setTurkAccount(String turkAccount) {
		this.turkAccount = turkAccount;
	}

	public boolean isRememberPassword() {
		return rememberPassword;
	}

	public void setRememberPassword(boolean rememberPassword) {
		this.rememberPassword = rememberPassword;
	}

    public int getServerUserId() {
        return serverUserId;
    }

    public void setServerUserId(int serverUserId) {
        this.serverUserId = serverUserId;
    }
}
