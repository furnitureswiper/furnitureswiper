package com.cymax.dataswipe.model;

import android.database.DataSetObservable;
import android.util.Log;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.field.DatabaseField;

import java.sql.SQLException;
import java.util.List;

public class DefaultData extends DataSetObservable {

	protected static final String EXPORTED_FIELD_NAME = "exported";
	
	@DatabaseField(generatedId = true, allowGeneratedIdInsert = true )
	protected int id;
	
	@DatabaseField(columnName = EXPORTED_FIELD_NAME)
	protected boolean exported;
	
	public boolean isExported() {
		return exported;
	}

	public void setExported(boolean exported) {
		this.exported = exported;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	protected void deleteAllExportedOverID(Dao<?,Integer> dao, int id) {
		try {
			List<?> items = dao.queryForEq(EXPORTED_FIELD_NAME,true);
			
			for(Object item : items)
			{
				if (((DefaultData)item).getId() > id)
				dao.deleteById(((DefaultData)item).getId());
			}
			
		} catch (SQLException e) {
			Log.e(DefaultData.class.getName(), "deleteAllExportedOverID", e);
		}
	}
}
