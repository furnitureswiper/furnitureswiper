package com.cymax.dataswipe.model;

import android.util.Log;

import com.cymax.dataswipe.db.DatabaseManager;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.sql.SQLException;
import java.util.List;

@DatabaseTable(tableName = "device")
public class DeviceData extends DefaultData {
	
	@DatabaseField(columnName = "mac_adress")
	private String macAddress;
	@DatabaseField(columnName = "database_version")
	private int databaseVersion;
	@DatabaseField(columnName = "server_address")
	private String serverAdress;
	
	public DeviceData (){}
	
	public String getMacAddress() {
		return macAddress;
	}	
	public void setMacAddress(String mac_address) {
		this.macAddress = mac_address;
	}

	public int getDatabaseVersion() {
		return databaseVersion;
	}

	public void setDatabaseVersion(int database_version) {
		this.databaseVersion = database_version;
	}

	public List<DeviceData> getAll() {
		List<DeviceData> devices = null;
		try {
			devices =  DatabaseManager.getDeviceDataDao().queryForAll();
		} catch (SQLException e) {
			Log.e(DeviceData.class.getName(), "getAll", e);
		}
		return devices;
	}
	
	public static DeviceData getById(int deviceId) {
		DeviceData device = null;
		try {
			device = DatabaseManager.getDeviceDataDao().queryForId(deviceId);
		} catch (SQLException e) {
			Log.e(DeviceData.class.getName(), "getById", e);
		}
		return device;
	}
	
	public void update() {
		try {
			DatabaseManager.getDeviceDataDao().update(this);
		} catch (SQLException e) {
			Log.e(DeviceData.class.getName(), "update", e);
		}
	}

    static public void deleteAll() {
        try {

            for(DeviceData item : DatabaseManager.getDeviceDataDao().queryForAll())
            {
                DatabaseManager.getDeviceDataDao().deleteById(item.getId());
            }

        } catch (SQLException e) {
            Log.e(DeviceData.class.getSimpleName(), "deleteAll", e);
        }
    }

	public String getServerAdress() {
		return serverAdress;
	}

	public String getUrlBaseAddress() {

        Log.e(DeviceData.class.getName(), String.format("http://%s/CymaxDCBackend/DCBackendService.svc", this.getServerAdress()));
		return String.format("http://%s/CymaxDCBackend/DCBackendService.svc", this.getServerAdress());
	}

	public void setServerAdress(String serverAdress) {
		this.serverAdress = serverAdress;
	}	

}
