package com.cymax.dataswipe.model;

import android.net.Uri;
import android.util.Log;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.cymax.dataswipe.AppController;
import com.cymax.dataswipe.MainActivity;
import com.cymax.dataswipe.adapter.AttributesAdapter;
import com.cymax.dataswipe.db.DatabaseManager;
import com.cymax.dataswipe.swipelistview.SwipeListView;
import com.j256.ormlite.field.DatabaseField;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Holds product data information
 */
public class ProductData extends DefaultData{

    @DatabaseField
    private int ProductId;
    @DatabaseField
    private String Name;
	@DatabaseField
    private String imageUrl;
	@DatabaseField
    private String description;
	@DatabaseField
    private String category;
    @DatabaseField
    private String attributeGroup;
	@DatabaseField
    private String attribute;

    private SwipeListView swipeListView;
    private AttributesAdapter attributesAdapter;
    private List<AttributeData> attributeDataList;
    private List<AttributeData> savedAttributesList;


	public ProductData() {
		// needed by ormlite
        attributeDataList = new ArrayList<AttributeData>();
        setSavedAttributesList(new ArrayList<AttributeData>());
	}

	public ProductData(String imageUrl, String description, String category, String attribute) {
		this.setImageUrl(imageUrl);
		this.setCategory(category);
		this.setAttribute(attribute);
	}

    @Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("id=").append(id);
		sb.append(", ").append("imageUrl=").append(getImageUrl());
		sb.append(", ").append("description=").append(getDescription());
		return sb.toString();
	}

    static public void retrieveDataFromServer(String category)
    {
        DeviceData dv = new DeviceData().getById(1);
        String url = dv.getUrlBaseAddress() + "/GetSwipeCards/" + Uri.encode(category) + "?testVar=1234";

        JsonArrayRequest req = new JsonArrayRequest(url,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        //Log.d(this.getClass().getSimpleName(), response.toString());

                        List<ProductData> items = new ArrayList<ProductData>();

                        try {
                            // Load product data from a JSON array in the response
                            String jsonResponse = "";
                            for (int i = 0; i < response.length(); i++) {

                                final JSONObject productData = (JSONObject)response.get(i);

                                ProductData item =  new ProductData(){{
                                    setProductId(productData.getInt("Id"));
                                    setName(productData.getString("Name"));
                                    setImageUrl(productData.getString("ImageUrl"));
                                    setDescription(productData.getString("Description"));
                                    setCategory(productData.getString("Category"));
                                    setAttributeGroup(productData.getString("AttributeGroup"));
                                    setAttribute(productData.getString("Attribute"));
                                }};

                                // Load category attributes into product data (acquiring data together for performance)
                                item.setAttributeDataList(new ArrayList<AttributeData>());
                                for (int b = 0; b < productData.getJSONArray("AttributeDataList").length(); b++) {
                                    final JSONObject attrData = (JSONObject)productData.getJSONArray("AttributeDataList").get(b);
                                    item.getAttributeDataList().add(new AttributeData(){{
                                        setAttributeGroup(attrData.getString("AttributeGroup"));
                                        setAttribute(attrData.getString("Attribute"));
                                        setCategory(attrData.getString("Category"));
                                        setParentCategory("ParentCategory");
                                    }});
                                }

                                if ( !items.contains(item) && item.insert() > 0 ) items.add(item);
                            }

                            if (MainActivity.swipeCardsAdapter != null) {
                                MainActivity.swipeCardsAdapter.addItems(items);
                                MainActivity.swipeCardsAdapter.notifyDataSetChanged();
                            }
                            else
                            {
                                if (MainActivity.al == null) MainActivity.al = new ArrayList<>();
                                MainActivity.al.addAll(items);
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                            VolleyLog.d(this.getClass().getSimpleName(), "Error: " + e.getMessage());
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(this.getClass().getSimpleName(), "Error: " + error.getMessage());
            }

        });

        req.setRetryPolicy(new DefaultRetryPolicy(
                60000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(req);
    }

    public static void removeAll() {
         MainActivity.al = new ArrayList<>();
    }

    public int insert() {
        try {
            DatabaseManager.getProductDataDao().create(this);
            return this.id;
        } catch (SQLException e) {
            Log.e(this.getClass().getSimpleName(), "insert", e);
            return -1;
        }
    }

    public int delete() {
        try {
            return DatabaseManager.getProductDataDao().deleteById(this.id);
        } catch (SQLException e) {
            Log.e(this.getClass().getSimpleName(), "insert", e);
            return -1;
        }
    }

    static public List<ProductData> getAll() {
        List<ProductData> result = new ArrayList<ProductData>();
        try {
            result = DatabaseManager.getProductDataDao().queryForAll();
        } catch (SQLException e) {
            Log.e(ProductData.class.getName(), "getAll", e);
        }
        return result;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getAttributeGroup() {
        return attributeGroup;
    }

    public void setAttributeGroup(String attributeGroup) {
        this.attributeGroup = attributeGroup;
    }

    public String getAttribute() {
        return attribute;
    }

    public void setAttribute(String attribute) {
        this.attribute = attribute;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public int getProductId() {
        return ProductId;
    }

    public void setProductId(int productId) {
        ProductId = productId;
    }

    public List<AttributeData> getAttributeDataList() {

        if (attributesAdapter != null) attributesAdapter.notifyDataSetChanged();
        return attributeDataList;
    }

    public void setAttributeDataList(List<AttributeData> attributeDataList) {
        this.attributeDataList = attributeDataList;
        if (attributesAdapter != null) attributesAdapter.notifyDataSetChanged();
    }

    public AttributesAdapter getAttributesAdapter() {
        if (attributesAdapter != null) attributesAdapter.notifyDataSetChanged();
        return attributesAdapter;
    }

    public void setAttributesAdapter(AttributesAdapter attributesAdapter) {
        this.attributesAdapter = attributesAdapter;
        attributesAdapter.notifyDataSetChanged();
    }

    public SwipeListView getSwipeListView() {
        if (attributesAdapter != null) attributesAdapter.notifyDataSetChanged();
        return swipeListView;
    }

    public void setSwipeListView(SwipeListView swipeListView) {
        this.swipeListView = swipeListView;
    }

    public List<AttributeData> getSavedAttributesList() {
        return savedAttributesList;
    }

    public void setSavedAttributesList(List<AttributeData> savedAttributesList) {
        this.savedAttributesList = savedAttributesList;
    }
}
