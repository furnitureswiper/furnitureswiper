package com.cymax.dataswipe.model;

import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.cymax.dataswipe.AppController;
import com.cymax.dataswipe.db.DatabaseManager;
import com.cymax.dataswipe.helper.Helper;
import com.j256.ormlite.field.DatabaseField;

import org.json.JSONObject;
import org.json.JSONStringer;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * A simple demonstration object we are creating and persisting to the database.
 */
public class ProductReviewData extends DefaultData{

    @DatabaseField
    private int userId;
	@DatabaseField
    private Date date;
	@DatabaseField
    private String macAddress;
	@DatabaseField
    private int productId;
    @DatabaseField
    private String attributeGroup;
	@DatabaseField
    private String attribute;

    private List<AttributeData> savedAttributes;


	public ProductReviewData() {
		// needed by ormlite
	}

	@Override
	public String toString() {
        return "prodid=" + productId +
                ", " + "attrgroup=" + attributeGroup +
                ", " + "attr=" + attribute;
	}

    static public void sendToServer(final ProductReviewData reviewData)
    {
        DeviceData dv = DeviceData.getById(1);
        String url = dv.getUrlBaseAddress() + "/SaveProductReview";
        final JSONObject entity;

        // Build JSON array
        JSONStringer reviewJObj;
        try {
            reviewJObj = new JSONStringer()
                        .object()
                            .key("reviewData")
                            //.array()
                                .object()
                                    .key("UserId").value( reviewData.getUserId() )
                                    .key("MacAddress").value(reviewData.getMacAddress())
                                    .key("ProductId").value(reviewData.getProductId())
                                    .key("AttributeGroup").value(reviewData.getAttributeGroup())
                                    .key("Attribute").value(reviewData.getAttribute())
                                .endObject()
                            //.endArray()
                            .key("savedAttributes")
                            .array();
                                for (AttributeData attrItem : reviewData.getSavedAttributes()) {
                                    reviewJObj.object()
                                            .key("Category").value( attrItem.getCategory() )
                                            .key("AttributeGroup").value(attrItem.getAttributeGroup())
                                            .key("ParentCategory").value(attrItem.getParentCategory())
                                            .key("Attribute").value(attrItem.getAttribute())
                                            .key("AttributeAdded").value(attrItem.isAttributeAdded())
                                    .endObject();
                                }
                            reviewJObj.endArray()
                        .endObject();

            entity = new JSONObject(reviewJObj.toString());

            JsonObjectRequest req;

            req = new JsonObjectRequest( Request.Method.POST, url,entity,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        VolleyLog.d(this.getClass().getSimpleName(), "Response: " + response.toString());
                    }
                }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    VolleyLog.d(this.getClass().getSimpleName(), "Error: " + error.getMessage());
                }
            }){
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    try {
                        Date date = new Date();
                        Map<String, String> headers = new HashMap<>();
                        headers.put("Authorization", Helper.generateRequestToken(date));
                        headers.put("Date", date.toString());
                        return headers;
                    } catch (Exception e) {
                        Log.e("Authorization", "Authentication Failure");
                    }
                    return super.getParams();
                }
            };

            req.setRetryPolicy(new DefaultRetryPolicy(
                    60000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            // Adding request to request queue
            AppController.getInstance().addToRequestQueue(req);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public int insert() {
        try {
            DatabaseManager.getProductReviewDataDao().create(this);
            return this.id;
        } catch (SQLException e) {
            Log.e(this.getClass().getSimpleName(), "insert", e);
            return -1;
        }
    }

    static public List<ProductReviewData> getAll() {
        List<ProductReviewData> result = new ArrayList<ProductReviewData>();
        try {
            result = DatabaseManager.getProductReviewDataDao().queryForAll();
        } catch (SQLException e) {
            Log.e(ProductReviewData.class.getName(), "getAll", e);
        }
        return result;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }
    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getMacAddress() {
        return macAddress;
    }

    public void setMacAddress(String macAddress) {
        this.macAddress = macAddress;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public String getAttributeGroup() {
        return attributeGroup;
    }

    public void setAttributeGroup(String attributeGroup) {
        this.attributeGroup = attributeGroup;
    }

    public String getAttribute() {
        return attribute;
    }

    public void setAttribute(String attribute) {
        this.attribute = attribute;
    }

    public List<AttributeData> getSavedAttributes() {
        return savedAttributes;
    }

    public void setSavedAttributes(List<AttributeData> savedAttributes) {
        this.savedAttributes = savedAttributes;
    }
}
