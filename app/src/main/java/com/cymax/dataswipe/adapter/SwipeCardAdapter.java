package com.cymax.dataswipe.adapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.graphics.Color;
import android.support.annotation.UiThread;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.text.InputType;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.cymax.dataswipe.MainActivity;
import com.cymax.dataswipe.R;
import com.cymax.dataswipe.model.AttributeData;
import com.cymax.dataswipe.model.ProductData;
import com.cymax.dataswipe.swipelistview.SwipeListView;
import com.cymax.dataswipe.swipelistview.SwipeMenu;
import com.cymax.dataswipe.swipelistview.SwipeMenuCreator;
import com.cymax.dataswipe.swipelistview.SwipeMenuItem;

import java.util.ArrayList;
import java.util.List;

public class SwipeCardAdapter extends BaseAdapter {

            public List<ProductData> productList;
            public Context context;

            public SwipeCardAdapter(List<ProductData> items, Context context) {
                this.productList = items;
                this.context = context;
            }

            public void addItems(List<ProductData> items){  this.productList.addAll(items);}

            @Override
            public int getCount() {
                return productList.size();
            }

            @Override
            public Object getItem(int position) {
                return position;
            }

            @Override
            public long getItemId(int position) {
                return position;
            }

            @Override
            public View getView(final int position, View convertView, ViewGroup parent) {

                View rowView = convertView;

                if (rowView == null) {

                    LayoutInflater inflater =   ((Activity)context).getLayoutInflater();
                    rowView = inflater.inflate(R.layout.item, parent, false);
                    // configure view holder
                    MainActivity.viewHolder = new MainActivity.ViewHolder();
                    MainActivity.viewHolder.ProductName = (TextView) rowView.findViewById(R.id.txtProductName);
                    MainActivity.viewHolder.CategoryText = (TextView) rowView.findViewById(R.id.txtCategory);
                    MainActivity.viewHolder.background = (RelativeLayout) rowView.findViewById(R.id.background);
                    MainActivity.viewHolder.infoButton = (ImageView) rowView.findViewById(R.id.infoBtn);
                    MainActivity.viewHolder.cardImage = (ImageView) rowView.findViewById(R.id.cardImage);
                    MainActivity.viewHolder.swipeListView = (SwipeListView) rowView.findViewById(R.id.listView);
                    productList.get(position).setSwipeListView((SwipeListView) rowView.findViewById(R.id.listView));
                    rowView.setTag(MainActivity.viewHolder);

                } else {
                    MainActivity.viewHolder = (MainActivity.ViewHolder) convertView.getTag();
                    MainActivity.viewHolder.swipeListView =  productList.get(position).getSwipeListView();
                    ((AttributesAdapter)MainActivity.viewHolder.swipeListView.getAdapter()).notifyDataSetChanged();
                }

                // Load controls in view with values
                MainActivity.viewHolder.ProductName.setText(productList.get(position).getName());
                MainActivity.viewHolder.CategoryText.setText(productList.get(position).getAttributeGroup());

                MainActivity.viewHolder.infoButton.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v)
                    {
                        MainActivity.viewHolder.webView = (WebView)MainActivity.webViewDialog.findViewById(R.id.wb_webview);
                        // Disable zoom
                        MainActivity.viewHolder.webView.getSettings().setBuiltInZoomControls(true);
                        //Scroll bars should not be hidden
                        MainActivity.viewHolder.webView.setScrollbarFadingEnabled(false);
                        //Disable the horizontal scroll bar
                        MainActivity.viewHolder.webView.setHorizontalScrollBarEnabled(false);
                        //Clear the cache
                        MainActivity.viewHolder.webView.setClickable(false);
                        MainActivity.viewHolder.webView.setClickable(false);
                        //Make the webview to load the specified URL
                        MainActivity.viewHolder.webView.loadDataWithBaseURL(null,productList.get(position).getDescription(), "text/html", "UTF-8", null);
                        MainActivity.viewHolder.webView.invalidate();
                        // Reset zoom (Only option on API14)
                        for(int i=0;i<8;i++)MainActivity.viewHolder.webView.zoomOut();
                        //Display the WebView dialog
                        MainActivity.webViewDialog.show();
                    }
                });

                MainActivity.addNewTag.setVisibility(View.VISIBLE);


                MainActivity.viewHolder.imageUrl = productList.get(position).getImageUrl();
                Glide.with(context).load(MainActivity.viewHolder.imageUrl).into(MainActivity.viewHolder.cardImage);

                productList.get(position).setAttributesAdapter(new AttributesAdapter(productList.get(position).getAttributeDataList(), context));
                productList.get(position).getSwipeListView().setAdapter(productList.get(position).getAttributesAdapter());
                productList.get(position).getAttributesAdapter().notifyDataSetChanged();

                // Set initial menu properties
                SwipeMenuCreator creator = new SwipeMenuCreator() {

                    @Override
                    public void create(SwipeMenu menu) {
                        // create "open" item
                        SwipeMenuItem openItem = new SwipeMenuItem(context);
                        // set item width
                        openItem.setWidth(dp2px(130));
                        // set item title
                        openItem.setTitle("ADD");
                        // set item title fontsize
                        openItem.setTitleSize(18);
                        // set item title font color
                        openItem.setTitleColor(Color.WHITE);
                        // add to menu
                        menu.addMenuItem(openItem);
                    }
                };
                // set creator
                productList.get(position).getSwipeListView().setMenuCreator(creator);


                // set SwipeListener
                productList.get(position).getSwipeListView().setOnSwipeListener(new SwipeListView.OnSwipeListener() {

                    @Override
                    public void onSwipeStart(int position) {

                    }

                    @Override
                    public void onSwipeEnd(int position) {
                    }
                });

                return rowView;
            }

            private int dp2px(int dp) {
                return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, context.getResources().getDisplayMetrics());
            }
        }