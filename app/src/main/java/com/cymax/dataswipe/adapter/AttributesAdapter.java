package com.cymax.dataswipe.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.cymax.dataswipe.R;
import com.cymax.dataswipe.model.AttributeData;

import java.util.List;

public class AttributesAdapter extends BaseAdapter {

    private List<AttributeData> items;
    public Context context;

    public AttributesAdapter(List<AttributeData> items, Context context) {
        this.setItems(items);
        this.context = context;
    }

    @Override
    public int getCount() {
        return getItems().size();
    }

    @Override
    public AttributeData getItem(int position) {
        return getItems().get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = View.inflate(this.context, R.layout.swipe_list_item, null);
            new ViewHolder(view);
        }
        ViewHolder holder = (ViewHolder)view.getTag();
        if (i >=0 && getCount() > i) {
            AttributeData item = getItem(i);
            holder.tv_name.setText(item.getAttribute());
        }
        else
        {
            holder.tv_name.setText("");
        }
        return view;
    }

//    public void removeItemAt(int position) {
//        getItems().remove(0);
//    }

    public List<AttributeData> getItems() {
        return items;
    }

    public void setItems(List<AttributeData> items) {
        this.items = items;
        this.notifyDataSetChanged();
        super.notifyDataSetChanged();
    }

    class ViewHolder {
        TextView tv_name;

        public ViewHolder(View view) {
            tv_name = (TextView) view.findViewById(R.id.tv_name);
            view.setTag(this);
        }
    }
}