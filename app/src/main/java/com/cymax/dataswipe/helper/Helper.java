package com.cymax.dataswipe.helper;

import android.util.Base64;

import java.math.BigInteger;
import java.security.Key;
import java.security.spec.KeySpec;
import java.util.Date;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

/**
 * Class to hold support methods
 */
public class Helper {

    private static final String FIXED_SALT = "CymaxStoresInc2016";
    private static final String IV = "Cym@x$tore$Inc16";

    public static String encryptedPassword(String raw)  {
        try
        {
            Cipher c = getCipher(Cipher.ENCRYPT_MODE, raw);

            byte[] encryptedVal = c.doFinal(raw.getBytes("UTF-8"));
            return  Base64.encodeToString(encryptedVal, Base64.DEFAULT).replaceAll("/","").replaceAll("\n","");
        }
        catch (Exception ex){
            ex.printStackTrace();
        }
        return raw;

    }

    public static String generateRequestToken(Date date)  {
        return encryptedPassword(encryptedPassword(FIXED_SALT + date.toString()) + date.toString());
    }

    private static Cipher getCipher(int mode, String password) throws Exception {
        Cipher c = Cipher.getInstance("AES/CBC/PKCS5Padding");

        //a random Init. Vector. just for testing
        byte[] iv = IV.getBytes("UTF-8");

        c.init(mode, generateKey(password), new IvParameterSpec(iv));
        return c;
    }

    private static Key generateKey(String password) throws Exception {
        SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
        byte[] salt = FIXED_SALT.getBytes("UTF-8");

        KeySpec spec = new PBEKeySpec(password.toCharArray(), salt, 2534, 128);
        SecretKey tmp = factory.generateSecret(spec);
        byte[] encoded = tmp.getEncoded();
        return new SecretKeySpec(encoded, "AES");
    }


}
