package com.cymax.dataswipe;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.cymax.dataswipe.helper.Helper;
import com.cymax.dataswipe.model.AttributeData;
import com.cymax.dataswipe.model.DeviceData;
import com.cymax.dataswipe.model.UserData;

import org.json.JSONObject;
import org.json.JSONStringer;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import java.io.StringReader;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

// Account Settings Activity
public class AccountSettingsActivity extends AppCompatActivity {
    EditText emailText;
    EditText turkText;
    EditText oldPass;
    EditText newPass;
    String oldemail;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account_setting);

        emailText = (EditText) findViewById(R.id.editTextEmail);
        turkText = (EditText) findViewById(R.id.editTextTurk);
        oldPass = (EditText) findViewById(R.id.editTextPassword);
        newPass = (EditText) findViewById(R.id.editTextNewPassword);
        UserData user = UserData.getById(1);
        emailText.setText(user.getUsername());
        turkText.setText(user.getTurkAccount());
        oldemail = user.getUsername();

        final Button confirm1 = (Button) findViewById(R.id.btnConfirm1);
        confirm1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            updateAcct(oldemail, emailText.getText().toString().trim(), turkText.getText().toString().trim());

            }
        });

        final Button confirm2 = (Button) findViewById(R.id.btnConfirm2);
        confirm2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
               updatePassword();
            }
        });
    }
    private void updateAcct(String oldemail, String email, String turk){

        String url = DeviceData.getById(1).getUrlBaseAddress()+"/AccountUpdate/" + oldemail + "/" + email + "/" + turk;

        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
                        DocumentBuilder builder;
                        InputSource is;
                        try {
                            UserData user = new UserData();
                            builder = factory.newDocumentBuilder();
                            is = new InputSource(new StringReader(response));
                            Document doc = builder.parse(is);
                            NodeList list = doc.getElementsByTagName("string");
                            String result = list.item(0).getTextContent();

                            if (result.equals("successful")) {
                                UserData.deletebyId(1);
                                user.setId(1);
                                user.setUsername(emailText.getText().toString().trim());
                                user.setTurkAccount(turkText.getText().toString().trim());
                                user.insert();

                                Toast.makeText(getApplicationContext(), "Update Successful!", Toast.LENGTH_LONG).show();
                                Intent intent = new Intent(AccountSettingsActivity.this, CategoryActitvity.class);
                                startActivity(intent);

                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), "error", Toast.LENGTH_LONG).show();
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                try {
                    Date date = new Date();
                    Map<String, String> headers = new HashMap<>();
                    headers.put("Authorization", Helper.generateRequestToken(date));
                    headers.put("Date", date.toString());
                    return headers;
                } catch (Exception e) {
                    Log.e("Authorization", "Authentication Failure");
                }
                return super.getParams();
            }
        };
        // Add the request to the RequestQueue.
        queue.add(stringRequest);
    }

    private void updatePassword() {
        String pass1 = oldPass.getText().toString().trim();
        String pass2 = newPass.getText().toString().trim();

        try {
            String encryptedPassword = Helper.encryptedPassword(pass1);
            String encryptedPassword2 = Helper.encryptedPassword(pass2);

            confirmPassUpdate(oldemail, encryptedPassword, encryptedPassword2);
        } catch (Exception ex) {
            Log.d("updatePassword ", ex.getMessage());
        }
    }

    private void confirmPassUpdate(String email, String pw1, String pw2) {

        DeviceData dv = DeviceData.getById(1);
        String url = dv.getUrlBaseAddress() + "/PasswordUpdate";
        final JSONObject entity;

        // Build JSON array
        JSONStringer reviewJObj;
        try {
            reviewJObj = new JSONStringer()
                    .object()
                        .key("email").value(email)
                        .key("oldpw").value(pw1)
                        .key("newpw").value(pw2)
                    .endObject();

            entity = new JSONObject(reviewJObj.toString());

            JsonObjectRequest req;

            req = new JsonObjectRequest( Request.Method.POST, url,entity,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            if (response.toString().contains("successful")) {
                                Toast.makeText(getApplicationContext(), "Password Updated", Toast.LENGTH_LONG).show();
                                Intent intent = new Intent(AccountSettingsActivity.this, CategoryActitvity.class);
                                startActivity(intent);

                            } else {
                                Toast.makeText(getApplicationContext(), "Invalid Password", Toast.LENGTH_LONG).show();
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(getApplicationContext(), "The server refused the new password!", Toast.LENGTH_LONG).show();
                }
            }){
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    try {
                        Date date = new Date();
                        Map<String, String> headers = new HashMap<>();
                        headers.put("Authorization", Helper.generateRequestToken(date));
                        headers.put("Date", date.toString());
                        return headers;
                    } catch (Exception e) {
                        Log.e("Authorization", "Authentication Failure");
                    }
                    return super.getParams();
                }
            };

            // Adding request to request queue
            AppController.getInstance().addToRequestQueue(req);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
