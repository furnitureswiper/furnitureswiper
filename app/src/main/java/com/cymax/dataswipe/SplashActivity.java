package com.cymax.dataswipe;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.cymax.dataswipe.db.DatabaseHelper;
import com.cymax.dataswipe.db.DatabaseManager;
import com.cymax.dataswipe.helper.Helper;
import com.cymax.dataswipe.model.DeviceData;
import com.cymax.dataswipe.model.UserData;
import com.j256.ormlite.android.apptools.OpenHelperManager;

import org.json.JSONObject;
import org.json.JSONStringer;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import java.io.StringReader;
import java.math.BigInteger;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

public class SplashActivity extends AppCompatActivity {

    private DatabaseHelper databaseHelper = null;
    private EditText editLogin;
    private EditText editPassword;
    private CheckBox chkRememberPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        DatabaseManager.init(this);

        editLogin = (EditText)findViewById(R.id.editLogin);
        editPassword = (EditText)findViewById(R.id.editPassword);
        chkRememberPassword = (CheckBox)findViewById(R.id.chkRememberPassword);

        // suggest the last logged user
        UserData lastUser = UserData.getById(1);
        if(lastUser != null)
        {
            editLogin.setText(UserData.getById(1).getUsername());
            if (lastUser.isRememberPassword()) {
                editPassword.setText(lastUser.getPassword());
                chkRememberPassword.setChecked(true);
            }
            else editPassword.requestFocus();
        }

        final Button button = (Button) findViewById(R.id.btnLogin);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (!isEmpty(editLogin, editPassword))
                    login();
            }
        });

        final TextView textView = (TextView) findViewById(R.id.textRegister);
        textView.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            Intent intent = new Intent(SplashActivity.this, Registration.class);
            startActivity(intent);
            }
        });

        final TextView textView2 = (TextView) findViewById(R.id.textReset);
        textView2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(SplashActivity.this, ResetPasswordEmail.class);
                startActivity(intent);
            }
        });
    }

    private void login() {
        String username = editLogin.getText().toString().trim();
        String password = editPassword.getText().toString().trim();

        userLogin(username, password, chkRememberPassword.isChecked());
    }

    private void userLogin(String username, final String password, final boolean rememberPassword) {

        DeviceData dv = DeviceData.getById(1);
        String url = dv.getUrlBaseAddress() + "/Login";
        final JSONObject entity;

        // Build JSON array
        JSONStringer reviewJObj;
        try {
            reviewJObj = new JSONStringer()
                    .object()
                    .key("email").value(username)
                    .key("password").value(Helper.encryptedPassword(password))
                    .endObject();

            entity = new JSONObject(reviewJObj.toString());

            JsonObjectRequest req;

            req = new JsonObjectRequest( Request.Method.POST, url,entity,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {

                            try {
                                String[] accountInfo = response.getString("result").split(",");
                                if (accountInfo.length == 3) {
                                    // Save user info in local db
                                    UserData.deletebyId(1);
                                    UserData user = new UserData();
                                    user.setId(1);
                                    user.setUsername(accountInfo[0]);
                                    user.setTurkAccount(accountInfo[1]);
                                    user.setServerUserId(Integer.parseInt(accountInfo[2]));
                                    user.setPassword(password);
                                    user.setRememberPassword(rememberPassword);
                                    user.insert();

                                    Toast.makeText(getApplicationContext(), "Login Successful", Toast.LENGTH_LONG).show();
                                    Intent intent = new Intent(SplashActivity.this, CategoryActitvity.class);
                                    startActivity(intent);

                                } else {
                                    Toast.makeText(getApplicationContext(), "Invalid Credential", Toast.LENGTH_LONG).show();
                                }
                            }
                            catch (Exception ex)
                            {
                                ex.printStackTrace();
                                Toast.makeText(getApplicationContext(), "Invalid server response", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(getApplicationContext(), "The server refused the new password!", Toast.LENGTH_LONG).show();
                }
            }){
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    try {
                        Date date = new Date();
                        Map<String, String> headers = new HashMap<>();
                        headers.put("Authorization", Helper.generateRequestToken(date));
                        headers.put("Date", date.toString());
                        return headers;
                    } catch (Exception e) {
                        Log.e("Authorization", "Authentication Failure");
                    }
                    return super.getParams();
                }
            };

            req.setRetryPolicy(new DefaultRetryPolicy(
                    6000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            // Adding request to request queue
            AppController.getInstance().addToRequestQueue(req);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Intialize databaseHelper class
     */
    private DatabaseHelper getHelper() {
        if (databaseHelper == null) {
            databaseHelper = OpenHelperManager.getHelper(this, DatabaseHelper.class);
        }
        return databaseHelper;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

		/*
         * Release databaseHelper
		 */
        if (databaseHelper != null) {
            OpenHelperManager.releaseHelper();
            databaseHelper = null;
        }
    }


    // check if edittext is empty
    private boolean isEmpty(EditText etText, EditText etText1) {
        if (etText.getText().toString().trim().length() == 0 || etText1.getText().toString().trim().length() == 0) {
            return true;
        } else {
            return false;
        }
    }
}
