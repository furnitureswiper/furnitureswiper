package com.cymax.dataswipe;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

public class GridviewAdapter extends BaseAdapter
{
    private ArrayList<String> listCategory;
    private Activity activity;

    public GridviewAdapter(Activity activity,ArrayList<String> listCategory) {
        super();
        this.listCategory = listCategory;
        this.activity = activity;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return listCategory.size();
    }

    @Override
    public String getItem(int position) {
        // TODO Auto-generated method stub
        return listCategory.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return 0;
    }

    public static class ViewHolder
    {
        public TextView txtViewTitle;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        ViewHolder view;
        LayoutInflater inflator = activity.getLayoutInflater();

        if(convertView==null)
        {
            view = new ViewHolder();
            convertView = inflator.inflate(R.layout.grid_row, null);

            view.txtViewTitle = (TextView) convertView.findViewById(R.id.textView1);

            convertView.setTag(view);
        }
        else
        {
            view = (ViewHolder) convertView.getTag();
        }

        view.txtViewTitle.setText(listCategory.get(position));

        return convertView;
    }
}