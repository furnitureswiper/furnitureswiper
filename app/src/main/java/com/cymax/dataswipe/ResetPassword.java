package com.cymax.dataswipe;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.cymax.dataswipe.helper.Helper;
import com.cymax.dataswipe.model.DeviceData;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import java.io.StringReader;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

public class ResetPassword extends AppCompatActivity implements View.OnClickListener {

    private Button btnReset;
    private EditText txtEmail;
    private EditText txtTempPassword;
    private EditText txtNewPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        ProgressDialog pDialog;

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password);

        txtEmail = (EditText) findViewById(R.id.txtEmail);
        txtTempPassword = (EditText) findViewById(R.id.txtTempPass);
        txtNewPassword = (EditText) findViewById(R.id.txtNewPass);

        Bundle b = getIntent().getExtras();
        String email = b.getString("email");
        txtEmail.setText(email);

        btnReset = (Button) findViewById(R.id.btnReset);
        btnReset.setOnClickListener(this);

        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Loading.....");
        pDialog.setCancelable(false);
    }

    @Override
    public void onClick(View v) {
        if (v == btnReset) {
            if (!isEmpty(txtEmail, txtTempPassword, txtNewPassword))
                resetPassword();
        }
    }

    private void resetPassword() {

        String email = txtEmail.getText().toString().trim();
        String temp = txtTempPassword.getText().toString().trim();
        String password = txtNewPassword.getText().toString().trim();

        String encryptedPassword = Helper.encryptedPassword(password);
        password(email, temp, encryptedPassword);
    }

    private void password(String email, String temp, String password) {

        String url = DeviceData.getById(1).getUrlBaseAddress() + "/ResetPassword/" + email + "/" + temp + "/" + password;

        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
                        DocumentBuilder builder;
                        InputSource is;
                        try {
                            builder = factory.newDocumentBuilder();
                            is = new InputSource(new StringReader(response));
                            Document doc = builder.parse(is);
                            NodeList list = doc.getElementsByTagName("string");
                            String result = list.item(0).getTextContent();

                            if (result.equals("successful")) {
                                Handler handler = new Handler();
                                handler.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        Intent intent = new Intent(ResetPassword.this, SplashActivity.class);
                                        startActivity(intent);
                                    }
                                }, 1500);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), "error", Toast.LENGTH_LONG).show();
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                try {
                    Date date = new Date();
                    Map<String, String> headers = new HashMap<>();
                    headers.put("Authorization", Helper.generateRequestToken(date));
                    headers.put("Date", date.toString());
                    return headers;
                } catch (Exception e) {
                    Log.e("Authorization", "Authentication Failure");
                }
                return super.getParams();
            }
        };

        // Add the request to the RequestQueue.
        queue.add(stringRequest);
    }


    // check if edittext is empty
    private boolean isEmpty(EditText etText, EditText etText1, EditText etText2) {
        return (etText.getText().toString().trim().length() == 0 || etText1.getText().toString().trim().length() == 0 || etText2.getText().toString().trim().length() == 0);
    }
}
