package com.cymax.dataswipe.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.cymax.dataswipe.model.DeviceData;
import com.cymax.dataswipe.model.ProductData;
import com.cymax.dataswipe.model.ProductReviewData;
import com.cymax.dataswipe.model.UserData;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;

public class DatabaseHelper extends OrmLiteSqliteOpenHelper {

	// name of the database file for the application
	private static final String DATABASE_NAME = "cymax.db";
	// any time you make changes to your database objects, you may have to increase the database version
	private static final int DATABASE_VERSION = 80;
			
	public DatabaseHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db, ConnectionSource connectionSource) {
		try {
			Log.i(DatabaseHelper.class.getName(), "onCreate");
			TableUtils.createTable(connectionSource, DeviceData.class);
			TableUtils.createTable(connectionSource, ProductData.class);
            TableUtils.createTable(connectionSource, ProductReviewData.class);
            TableUtils.createTable(connectionSource, UserData.class);

			insertInitialData();

		} catch (SQLException e) {
			Log.e(DatabaseHelper.class.getName(), "Can't create database", e);
			throw new RuntimeException(e);
		}
	}
	
	@Override
	public void onUpgrade(SQLiteDatabase db, ConnectionSource connectionSource, int oldVersion, int newVersion) {
		try {
			Log.i(DatabaseHelper.class.getName(), "onUpgrade");
			TableUtils.dropTable(connectionSource, DeviceData.class, true);
			TableUtils.dropTable(connectionSource, ProductData.class, true);
            TableUtils.dropTable(connectionSource, ProductReviewData.class, true);
            TableUtils.dropTable(connectionSource, UserData.class, true);

			// after we drop the old databases, we create the new ones
			onCreate(db, connectionSource);
		} catch (SQLException e) {
			Log.e(DatabaseHelper.class.getName(), "Can't drop databases", e);
			throw new RuntimeException(e);
		}	
	}
	
	private void insertInitialData() throws SQLException {
		
		// Device
		//DeviceData.deleteAll();
		DeviceData device = new DeviceData();
		device.setId(1);
		device.setMacAddress("");
		device.setDatabaseVersion(DATABASE_VERSION);
		//device.setServerAdress("192.168.1.73");
		device.setServerAdress("192.168.3.29");
		getDeviceDataDao().create(device);

	}  	

	// Devices
	private Dao<DeviceData, Integer> deviceDao = null;
	public Dao<DeviceData, Integer> getDeviceDataDao() {
		if (null == deviceDao) {
			try {
				deviceDao = getDao(DeviceData.class);
			}catch (Exception e) {
				e.printStackTrace();
			}
		}
		return deviceDao;
	}
	
	// Products
	private Dao<ProductData, Integer> productDao = null;
	public Dao<ProductData, Integer> getProductDataDao() {
		if (null == productDao) {
			try {
				productDao = getDao(ProductData.class);
			}catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return productDao;
	}

    // Products Reviews
    private Dao<ProductReviewData, Integer> productReviewDao = null;
    public Dao<ProductReviewData, Integer> getProductsReviewDataDao() {
        if (null == productReviewDao) {
            try {
                productReviewDao = getDao(ProductReviewData.class);
            }catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return productReviewDao;
    }

    // Products Reviews
    private Dao<UserData, Integer> userDao = null;
    public Dao<UserData, Integer> getUserDataDao() {
        if (null == userDao) {
            try {
                userDao = getDao(UserData.class);
            }catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return userDao;
    }

	@Override
	public void close() {
		super.close();
	}
}
