package com.cymax.dataswipe.db;


import android.content.Context;

import com.cymax.dataswipe.model.DeviceData;
import com.cymax.dataswipe.model.ProductData;
import com.cymax.dataswipe.model.ProductReviewData;
import com.cymax.dataswipe.model.UserData;
import com.j256.ormlite.dao.Dao;

public class DatabaseManager {

	private static DatabaseManager instance;
	private static DatabaseHelper helper;
	
	static public void init(Context ctx) {
		if (null==instance) {
			instance = new DatabaseManager(ctx);
		}		
	}

	private DatabaseManager(Context ctx) {
		helper = new DatabaseHelper(ctx);
	}
	
	static public DatabaseManager getInstance() {
		return instance;
	}

	public static Dao<DeviceData, Integer> getDeviceDataDao() {
		return helper.getDeviceDataDao();
	}

	public static Dao<ProductData, Integer> getProductDataDao() {
		return helper.getProductDataDao();
	}

	public static Dao<ProductReviewData, Integer> getProductReviewDataDao() {
		return helper.getProductsReviewDataDao();
	}

    public static Dao<UserData, Integer> getUserDataDao() {
        return helper.getUserDataDao();
    }

}