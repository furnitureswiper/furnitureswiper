package com.cymax.dataswipe;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.cymax.dataswipe.model.ProductData;

import java.util.ArrayList;

public class CategoryActitvity extends Activity {
    /** Called when the activity is first created. */
    public static final String SELECTED_CATEGORY = "SELECTED_CATEGORY";
    private GridviewAdapter mAdapter;
    private ArrayList<String> listCategry;

    private GridView gridView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category);

        prepareList();

        final LinearLayout accountSettingsLink = (LinearLayout) findViewById(R.id.lin_account_settings_link);
        accountSettingsLink.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(CategoryActitvity.this, AccountSettingsActivity.class);
                startActivity(intent);
            }
        });
        // prepared arraylist and passed it to the Adapter class
        mAdapter = new GridviewAdapter(this,listCategry);

        // Set custom adapter to gridview
        gridView = (GridView) findViewById(R.id.gridView);
        gridView.setAdapter(mAdapter);

        // Implement On Item click listener
        gridView.setOnItemClickListener(new OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int position,
                                    long arg3) {

           ProductData.removeAll();
            // Request initial cards load while user still on login screen
            // to make application look smoothier (need to be adapted after categories screen is created)
            ProductData.retrieveDataFromServer(mAdapter.getItem(position));

            Intent intent = new Intent(CategoryActitvity.this,MainActivity.class);
            intent.putExtra(SELECTED_CATEGORY, mAdapter.getItem(position));
            startActivity(intent);
            Toast.makeText(CategoryActitvity.this, mAdapter.getItem(position), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void prepareList()
    {
        listCategry = new ArrayList<String>();

        listCategry.add("All");
        listCategry.add("Accessories");
        listCategry.add("Art & Drafting School Supplies");
        listCategry.add("Baby Furniture");
        listCategry.add("Bar and Game Room");
        listCategry.add("Bathroom Furniture");
        listCategry.add("Bedroom Furniture");
        listCategry.add("Cleaning & Breakroom");
        listCategry.add("Clocks");
        listCategry.add("Computer & Printer Accessories");
        listCategry.add("Daybeds and Accessories");
        listCategry.add("Decorative Accents");
        listCategry.add("Desk Accessories");
        listCategry.add("Desks");
        listCategry.add("Entertainment Furniture");
        listCategry.add("Entryway Furniture");
        listCategry.add("Filing & Storage");
        listCategry.add("Fireplace Accessories");
        listCategry.add("Fireplaces & Heaters");
        listCategry.add("Furniture");
        listCategry.add("Futons & Futon Accessories");
        listCategry.add("General Office Supplies");
        listCategry.add("Home Decor");
        listCategry.add("Housewares");
        listCategry.add("Kids");
        listCategry.add("Kids Furniture");
        listCategry.add("Kitchen and Dining");
        listCategry.add("LAN Management Systems");
        listCategry.add("Lighting");
        listCategry.add("Living Room");
        listCategry.add("Mailroom & Shipping");
        listCategry.add("Office Chairs & More");
        listCategry.add("Office Furniture");
        listCategry.add("Office Supplies");
        listCategry.add("Ornaments");
        listCategry.add("Outdoor Living");
        listCategry.add("Patio Furniture and Outdoor Furniture");
        listCategry.add("Play Furniture");
        listCategry.add("Presentation & Audiovisual");
        listCategry.add("Reception Furniture");
        listCategry.add("Small Appliances");
        listCategry.add("Small Storage");
        listCategry.add("Storage");
        listCategry.add("Tables");
        listCategry.add("Wall Art");
        listCategry.add("Yard and Garden");

    }
}