    package com.cymax.dataswipe;

    import android.app.Dialog;
    import android.content.DialogInterface;
    import android.graphics.Color;
    import android.graphics.drawable.ColorDrawable;
    import android.net.Uri;
    import android.os.Bundle;
    import android.support.annotation.UiThread;
    import android.support.v7.app.AlertDialog;
    import android.support.v7.app.AppCompatActivity;
    import android.text.InputType;
    import android.util.Log;
    import android.view.View;
    import android.view.Window;
    import android.view.WindowManager;
    import android.webkit.WebView;
    import android.widget.Button;
    import android.widget.EditText;
    import android.widget.ImageView;
    import android.widget.RelativeLayout;
    import android.widget.TextView;
    import android.widget.Toast;

    import com.cymax.dataswipe.adapter.*;
    import com.cymax.dataswipe.model.AttributeData;
    import com.cymax.dataswipe.model.DeviceData;
    import com.cymax.dataswipe.model.ProductData;
    import com.cymax.dataswipe.model.ProductReviewData;
    import com.cymax.dataswipe.model.UserData;
    import com.cymax.dataswipe.swipecard.FlingCardListener;
    import com.cymax.dataswipe.swipecard.SwipeFlingAdapterView;
    import com.cymax.dataswipe.swipelistview.SwipeListView;
    import com.cymax.dataswipe.swipelistview.SwipeMenuLayout;
    import com.google.android.gms.appindexing.Action;
    import com.google.android.gms.appindexing.AppIndex;
    import com.google.android.gms.common.api.GoogleApiClient;

    import java.util.ArrayList;
    import java.util.Date;
    import java.util.List;

    public class MainActivity extends AppCompatActivity implements FlingCardListener.ActionDownInterface {

        public static Dialog webViewDialog;
        public static TextView addNewTag;
        public static SwipeCardAdapter swipeCardsAdapter;
        public static ViewHolder viewHolder;
        public static List<ProductData> al;
        private SwipeFlingAdapterView flingContainer;
        private String currentCategory;
        private DeviceData deviceData;
        private UserData userData;
        /**
         * ATTENTION: This was auto-generated to implement the App Indexing API.
         * See https://g.co/AppIndexing/AndroidStudio for more information.
         */
        private GoogleApiClient client;

        public static void removeBackground() {

            ViewHolder.background.setVisibility(View.GONE);
            swipeCardsAdapter.notifyDataSetChanged();
        }

        @Override
        public void onResume() {
            super.onResume();
        }

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_main);

            currentCategory = getIntent().getStringExtra(CategoryActitvity.SELECTED_CATEGORY);

            // Create dialods for details
            webViewDialog = new Dialog(MainActivity.this);
            //Remove the dialog's title
            webViewDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            //Inflate the contents of this dialog with the Views defined at 'webviewdialog.xml'
            webViewDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            webViewDialog.setContentView(R.layout.activity_webview);
            //With this line, the dialog can be dismissed by pressing the back key
            webViewDialog.setCancelable(true);
            webViewDialog.setCanceledOnTouchOutside(true);
            //Initialize the Button object with the data from the 'webviewdialog.xml' file
            Button btClose = (Button) webViewDialog.findViewById(R.id.bt_close);
            //Define what should happen when the close button is pressed.
            btClose.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v){

                //Dismiss the dialog
                webViewDialog.hide();

                // clean dialog
                viewHolder.webView.loadDataWithBaseURL(null,"<html><body></body></html>", "text/html", "UTF-8", null);

                // Reset zoom (Only option on API14)
                for(int i=0;i<8;i++)MainActivity.viewHolder.webView.zoomOut();

                }
            });

            // Get User anda Device info
            userData = UserData.getById(1);
            deviceData = DeviceData.getById(1);

            //  Add new tag link
            addNewTag = (TextView)findViewById(R.id.lblAddNewTag);

            // Initiate adapters
            flingContainer = (SwipeFlingAdapterView) findViewById(R.id.frame);
            if (al == null) al = new ArrayList<ProductData>();

            // Instantiate adapter for swiping cards
            swipeCardsAdapter = new SwipeCardAdapter(al, MainActivity.this);
            flingContainer.setAdapter(swipeCardsAdapter);

            flingContainer.setFlingListener(new SwipeFlingAdapterView.onFlingListener() {
                @Override
                public void removeFirstObjectInAdapter() {

                }

                @Override
                public void onLeftCardExit(Object dataObject) {
                    al.get(0).delete();
                    al.remove(0);
                    swipeCardsAdapter.notifyDataSetChanged();

                    if(al.size()==0) addNewTag.setVisibility(View.GONE);
                }

                @Override
                public void onRightCardExit(Object dataObject) {
                    final ProductData currentProduct = al.get(0);
                    ProductReviewData reviewData = new ProductReviewData(){
                        {
                            setUserId(userData.getServerUserId());
                            setDate(new Date());
                            setMacAddress(deviceData.getMacAddress());
                            setProductId(currentProduct.getProductId());
                            setAttributeGroup(currentProduct.getAttributeGroup());
                            setAttribute(currentProduct.getAttribute());
                            setSavedAttributes(currentProduct.getSavedAttributesList());
                        }
                    };

                    ProductReviewData.sendToServer(reviewData);
                    currentProduct.delete();
                    al.remove(0);
                    swipeCardsAdapter.notifyDataSetChanged();
                    if(al.size()==0) addNewTag.setVisibility(View.GONE);
                }

                @Override
                public void onAdapterAboutToEmpty(int itemsInAdapter) {
                    ProductData.retrieveDataFromServer(currentCategory);
                    swipeCardsAdapter.notifyDataSetChanged();
                }

                @Override
                public void onScroll(float scrollProgressPercent) {
                    // Ignore animation errors if user sends multiple taps randomly
                    try {
                        View view = flingContainer.getSelectedView();
                        view.findViewById(R.id.item_swipe_right_indicator).setAlpha(scrollProgressPercent < 0 ? -scrollProgressPercent : 0);
                        view.findViewById(R.id.item_swipe_left_indicator).setAlpha(scrollProgressPercent > 0 ? scrollProgressPercent : 0);
                    }
                    catch(Exception ex){
                        ex.printStackTrace();
                    }
                }
            });

            // Optionally add an OnItemClickListener
            flingContainer.setOnItemClickListener(new SwipeFlingAdapterView.OnItemClickListener() {
                @Override
                public void onItemClicked(int itemPosition, Object dataObject) {
                    viewHolder.webView = (WebView) webViewDialog.findViewById(R.id.wb_webview);
                    // Enable zoom
                    viewHolder.webView.getSettings().setBuiltInZoomControls(true);
                    //Scroll bars should not be hidden
                    viewHolder.webView.setScrollbarFadingEnabled(true);
                    //Disable the horizontal scroll bar
                    viewHolder.webView.setHorizontalScrollBarEnabled(false);
                    viewHolder.webView.setBackgroundColor(Color.TRANSPARENT);
                    viewHolder.webView.setClickable(false);
                    //Make the webview to load the specified URL
                    //viewHolder.webView.loadDataWithBaseURL(null,productList.get(position).getDescription(), "text/html", "UTF-8", null);
                    viewHolder.webView.loadDataWithBaseURL(null,"<img src='" + al.get(0).getImageUrl() + " ' alt='' height='450px'/>", "text/html", "UTF-8", null);
                    viewHolder.webView.invalidate();
                    //Display the WebView dialog
                    webViewDialog.show();
                }
            });

            // Creates reusable add tag button
            addNewTag.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                    builder.setTitle("Add custom tag");

                    // Set up the input
                    final EditText input = new EditText(MainActivity.this);
                    input.setTextColor(Color.BLACK);
                    input.setPadding(10,10,10,10);

                    input.setBackgroundColor(Color.LTGRAY);
                    input.setHintTextColor(Color.BLACK);
                    input.setCursorVisible(true);
                    // Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
                    input.setInputType(InputType.TYPE_TEXT_FLAG_AUTO_CORRECT | InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_WORDS);
                    builder.setView(input);

                    // Set up the buttons
                    builder.setPositiveButton("Add", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            //m_Text = input.getText().toString();
                            al.get(0).getSavedAttributesList().add(new AttributeData(){{
                                setAttributeAdded(true);
                                setAttribute(input.getText().toString());
                                setAttributeGroup(al.get(0).getAttributeGroup());
                                setParentCategory(al.get(0).getCategory());
                                setCategory(al.get(0).getCategory());
                            }});
                            Toast.makeText(MainActivity.this,"Custom attribute added",Toast.LENGTH_SHORT).show();
                        }
                    });
                    builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });

                    final Dialog dialog = builder.create();

                    input.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                        @Override
                        public void onFocusChange(View v, boolean hasFocus) {
                            if (hasFocus) {
                                dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
                            }
                        }
                    });
                    dialog.show();
                    dialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE  | WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
                    input.requestFocus();
                }
            });


            // ATTENTION: This was auto-generated to implement the App Indexing API.
            // See https://g.co/AppIndexing/AndroidStudio for more information.
            client = new GoogleApiClient.Builder(MainActivity.this).addApi(AppIndex.API).build();
        }

        @Override
        public void onActionDownPerform() {
            Log.e("action", "bingo");
        }

        @Override
        public void onStart() {
            super.onStart();

            // ATTENTION: This was auto-generated to implement the App Indexing API.
            // See https://g.co/AppIndexing/AndroidStudio for more information.
            client.connect();
            Action viewAction = Action.newAction(
                    Action.TYPE_VIEW, // TODO: choose an action type.
                    "Main Page", // TODO: Define a title for the content shown.
                    // TODO: If you have web page content that matches this app activity's content,
                    // make sure this auto-generated web page URL is correct.
                    // Otherwise, set the URL to null.
                    Uri.parse("http://host/path"),
                    // TODO: Make sure this auto-generated app URL is correct.
                    Uri.parse("android-app://com.cymax.dataswipe/http/host/path")
            );
            AppIndex.AppIndexApi.start(client, viewAction);
        }

        @Override
        public void onStop() {
            super.onStop();

            // ATTENTION: This was auto-generated to implement the App Indexing API.
            // See https://g.co/AppIndexing/AndroidStudio for more information.
            Action viewAction = Action.newAction(
                    Action.TYPE_VIEW, // TODO: choose an action type.
                    "Main Page", // TODO: Define a title for the content shown.
                    // TODO: If you have web page content that matches this app activity's content,
                    // make sure this auto-generated web page URL is correct.
                    // Otherwise, set the URL to null.
                    Uri.parse("http://host/path"),
                    // TODO: Make sure this auto-generated app URL is correct.
                    Uri.parse("android-app://com.cymax.dataswipe/http/host/path")
            );
            AppIndex.AppIndexApi.end(client, viewAction);
            client.disconnect();
        }

        public static class ViewHolder {
            public static RelativeLayout background;
            public TextView ProductName;
            public TextView CategoryText;
            public ImageView cardImage;
            public ImageView infoButton;
            public WebView webView;
            public String imageUrl;
            public SwipeListView swipeListView;
        }

        @UiThread
        public  void saveAttributeListItem(int position, boolean addAttribute) {

            // Add attribute to the list of attributes to send to server
            AttributeData savedAttribute = al.get(0).getAttributeDataList().get(position);
            savedAttribute.setAttributeAdded(addAttribute);
            al.get(0).getSavedAttributesList().add(savedAttribute);

            // Remove attribute from original list
            al.get(0).getAttributeDataList().remove(position);
            ((AttributesAdapter)al.get(0).getSwipeListView().getAdapter()).notifyDataSetChanged();

            int i=0;

            // Force redraw of list items
            try{

                View view;
                int itemPosition;
                for(; i <= al.get(0).getSwipeListView().getChildCount()+2; i++)
                {
                    if(al.get(0).getSwipeListView().getChildAt(i) != null)
                    {
                        view = ((SwipeMenuLayout)al.get(0).getSwipeListView().getChildAt(i)).getChildAt(0);
                        itemPosition = ((SwipeMenuLayout)al.get(0).getSwipeListView().getChildAt(i)).getPosition();
                    }
                    else
                    {
                        view = null;
                        itemPosition = -1;
                    }
                    al.get(0).getSwipeListView().getAdapter().getView(itemPosition, view, al.get(0).getSwipeListView());
                }
            }
            catch(Exception ex){
                ex.printStackTrace();
            }
        }

    }
